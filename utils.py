import json

def get_option(ents,triples):
    nodes = set()
    for ent in ents:
        nodes.add(ent)

    triple_dict = {}
    for triple in triples:
        ht = triple[0],triple[2]
        if ht not in triple_dict:
            triple_dict[ht] = []
        triple_dict[ht].append(triple[1])
        nodes.add(triple[0])
        nodes.add(triple[2])
    
    with open('option.json','r',encoding='utf-8') as f:
        option = json.load(f)
        for node in nodes:
            option['series'][0]['data'].append({'name':node})
        for (head,tail) in triple_dict:
            if head > tail:
                curveness = 0.2
            else:
                curveness = -0.2
            if '反向' in triple_dict[(head,tail)] or \
                '实体-病历共现' in triple_dict[(head,tail)] or \
                '疾病->逻辑症状' in triple_dict[(head,tail)] or \
                '其他节点->病历' in triple_dict[(head,tail)]:
                continue
            option['series'][0]['links'].append(
                {
                    "source": head,
                    "target": tail,
                    "symbolSize": [
                        5,
                        20
                    ],
                    "label": {
                        # "show": True,
                        "formatter": '/'.join(triple_dict[(head,tail)])
                    },
                    "lineStyle": {
                        "width": 5,
                        "curveness": curveness
                    }
                }
            )
    return option
    